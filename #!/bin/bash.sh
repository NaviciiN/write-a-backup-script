#!/bin/bash

# Get the source and target directories from the command line arguments
SOURCE_DIR="$1"
TARGET_DIR="$2"

# Get the current timestamp in the format of YYYY-MM-DD_HH-MM-SS
TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)

# Check if the target directory is local or remote
if [[ "$TARGET_DIR" == *":"* ]]; then
  # Remote backup using ssh
  ssh "${TARGET_DIR%%:*}" "mkdir -p ${TARGET_DIR#*:}/$TIMESTAMP"
  rsync -avz --link-dest=../current "$SOURCE_DIR" "${TARGET_DIR%/}/$TIMESTAMP/"
  ssh "${TARGET_DIR%%:*}" "cd ${TARGET_DIR#*:} && rm -f current && ln -s $TIMESTAMP current"
else
  # Local backup
  mkdir -p "$TARGET_DIR/$TIMESTAMP"
  rsync -avz --link-dest=../current "$SOURCE_DIR" "$TARGET_DIR/$TIMESTAMP/"
  cd "$TARGET_DIR" && rm -f current && ln -s "$TIMESTAMP" current
fi
